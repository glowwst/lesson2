import Foundation

/* 1) Напишите функцию в которую будет поступать строка в качестве параметра String,
 подсчитайте количество букв в строке и умножте на число которое будет находится вне функции,
 результат вернуть с функции и разделить на 2. */ 
 
let imputStrint: String = "ngq sa"
let someNumber: Double = 3

func countLetters(input: String) -> Double {
    var countLetters = 0
    for i in input {
        switch i {
            case "a"..."z":
                countLetters += 1
            default:
                ()
        }

    }
    var finalNumber = Double(countLetters) * someNumber
    return finalNumber
}

print(countLetters(input: imputStrint)/2)


/* 2) Cоздайте массив кортежей(тюплов), в которых будет первое поле имя студента, второе поле возраст студента, 
третье поле его оценка(от 0 до 10).  Заполните 20 значениями.

Используя функции высшего порядка:
1. Отсортируйте студентов по возрасту в порядке возрастания.
2. Покажите студентов которые старше 18 лет.
3. Найдите студентов у которых в имени имеются буквы a, ar, ga.
4. Уберите из массива стдуентов которые не имеют возраст(nil)
5. Сконвертируйте поле возраста у студента с типа данных Int в тип данных String и добавьте в конце слово "year" к возрасту.
6. Отсортируйте студентов по длинне их имени(начиная от самого большого до маленького) */ 


// Cоздайте массив кортеже
var studentTuple:[(fio: String, age: Int?, rating:Int)] = []
studentTuple.append((fio: "aTest1", age: nil, rating: 10))
studentTuple.append((fio: "test2", age: 12, rating: 5))
studentTuple.append((fio: "esgat3", age: 15, rating: 3))
studentTuple.append((fio: "dTeast4", age: nil, rating: 4))
studentTuple.append((fio: "Tesgat5", age: 18, rating: 10))
studentTuple.append((fio: "Test6", age: nil, rating: 5))
studentTuple.append((fio: "Tesat7", age: 20, rating: 6))
studentTuple.append((fio: "fTest8", age: nil, rating: 6))
studentTuple.append((fio: "Test9", age: 21, rating: 4))
studentTuple.append((fio: "aTesart10", age: 36, rating: 9))
studentTuple.append((fio: "Tesat11", age: 43, rating: 5))
studentTuple.append((fio: "Tesgat12", age: 23, rating: 4))
studentTuple.append((fio: "Test13", age: 43, rating: 3))
studentTuple.append((fio: "dTesart14", age: 21, rating: 10))
studentTuple.append((fio: "Test15", age: 24, rating: 9))
studentTuple.append((fio: "fTest16", age: 34, rating: 10))
studentTuple.append((fio: "Tesgat17", age: 32, rating: 3))
studentTuple.append((fio: "fTestga18", age: 21, rating: 10))
studentTuple.append((fio: "aTesgat19", age: 11, rating: 4))
studentTuple.append((fio: "Test20", age: 22, rating: 10))

// 1. Отсортируйте студентов по возрасту в порядке возрастания.
let sortAbsAge =  studentTuple.sorted(by: { ($1.age ?? 0) > ($0.age ?? 0)})
print(sortAbsAge)

// 2. Покажите студентов которые старше 18 лет.
let filtredByAge =  studentTuple.filter({ (($0.age ?? 0) > 18)})
print(filtredByAge)

// 3 Найдите студентов у которых в имени имеются буквы a, ar, ga.
let filtredByFio =  studentTuple.filter( {($0.fio.contains("ga"))||($0.fio.contains("ar"))||($0.fio.contains("a"))})
print(filtredByFio)

// 4 Уберите из массива стдуентов которые не имеют возраст(nil)
let TupleWichoutNil =  studentTuple.filter( {$0.age != nil} )
print(TupleWichoutNil)

// 5. Сконвертируйте поле возраста у студента с типа данных Int в тип данных String и добавьте в конце слово "year" к возрасту.

var convertStudentTuple:[(fio: String, age: String, rating:Int)] = []

for i in studentTuple {
    convertStudentTuple.append((fio: i.fio, age: String(i.age ?? 0) + " year", rating: i.rating))
}
print (convertStudentTuple);


// 6. Отсортируйте студентов по длинне их имени(начиная от самого большого до маленького) 
let sortAbsSortFIO =  studentTuple.sorted(by: { $0.fio.count > $1.fio.count })
print(sortAbsSortFIO)